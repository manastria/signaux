CC=gcc
CFLAGS=-W -Wall 
LDFLAGS=

all: signal

signal: signal.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -rf *.o