#!/bin/sh

git config user.name jpdemory
git config user.email jpdemory@manastria.fr

git config core.filemode true
git config core.ignorecase false
git config core.autocrlf input
git config push.default simple

# git config credential.helper 'cache --timeout=1800'  # en secondes 1800 = 30 minutes
