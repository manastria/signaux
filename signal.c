#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <ctype.h>

static void signal_handler (int);
int i, pid, status;
int cflag = 0;


void
printHeure ()
{
  time_t timer;
  char buffer[26];
  struct tm *tm_info;

  time (&timer);
  tm_info = localtime (&timer);

  strftime (buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);
  puts (buffer);
}


int
main (int argc, char *argv[], char *env[])
{
	int c;

	while ((c = getopt(argc, argv, "c")) != -1)
	{
		switch(c)
		{
			case 'c':
				cflag = 1;
				break;
/*			default:
				abort();*/
		}
	}

  if (signal (SIGUSR1, signal_handler) == SIG_ERR)
    {
      perror ("Unable to create handler for SIGUSR1\n");
    }

  if (signal (SIGUSR2, signal_handler) == SIG_ERR)
    {
      perror ("Unable to create handler for SIGUSR2\n");
    }

  if (cflag)
  {
	if (signal (SIGINT, signal_handler) == SIG_ERR)
    {
      perror ("Unable to create handler for SIGUSR2\n");
    }
  }

  if (getpgrp () == tcgetpgrp (STDOUT_FILENO))
    {
      printf ("foreground\n");
    }
  else
    {
      printf ("background\n");
    }


  if (isatty (1))
    fprintf (stdout, "Outputting to a terminal.");
  else
    fprintf (stdout, "Not outputting to a terminal.");

  printHeure ();
  printf ("pid = %d\n", pid = getpid ());

  for (;;)			/* loop forever */
    {
      printf ("%d : Bonjour\n", pid);
      sleep (2);
    }


  return 0;
}

static void
signal_handler (int signo)
{

  /* signo contains the signal number that was received */
  switch (signo)
    {
      /* Signal is a SIGUSR1 */
    case SIGUSR1:
      printf ("Process %d: SIGUSR1 recu\n", getpid ());
      break;

      /*  It's a SIGUSR2 */
    case SIGUSR2:
      printf ("Process %d: SIGUSR2 recu\n", getpid ());
      break;

    case SIGINT:
      printf ("Process %d: vous m'avez tué !!!!\n", getpid ());
      /*
         sleep(1);
         printf ("Process %d: Je rends l'ame !!!!\n", getpid ());
       */
      exit (1);
      break;
    default:
      break;
    }

  return;
}
